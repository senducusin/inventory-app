//
//  AddItemTableViewController.swift
//  Inventory App
//
//  Created by Jansen Ducusin on 2/26/21.
//

import Foundation
import UIKit

protocol  AddItemTableViewControlDelegate {
    func addItemTableViewControllerDidCancel(controller: UIViewController)
    func addItemTableViewControllerDidSave(controller: UIViewController, item:Item)
}

class AddItemTableViewController: UITableViewController{
    @IBOutlet weak var titleTextField: UITextField!
    
    var delegate: AddItemTableViewControlDelegate?
    
    @IBAction func save(){
        if let itemName = self.titleTextField.text {
            let inventoryItem = Item(name: itemName)
            if let delegate = self.delegate {
                delegate.addItemTableViewControllerDidSave(controller: self, item: inventoryItem)
            }
            
        }
    }
    
    @IBAction func cancel(){
        if let delegate = self.delegate {
            delegate.addItemTableViewControllerDidCancel(controller: self)
        }
    }
}
