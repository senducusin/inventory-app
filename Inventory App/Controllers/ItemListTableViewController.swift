//
//  ItemListTableViewController.swift
//  Inventory App
//
//  Created by Jansen Ducusin on 2/26/21.
//

import Foundation
import UIKit

class ItemListTableViewController: UITableViewController{
    
    var inventoryList:Group!
    private var itemListTableVMV: ItemListTableViewModelView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.itemListTableVMV = ItemListTableViewModelView(inventory: inventoryList)
        
        self.setupUI()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddItemTableViewController"{
            guard let navC = segue.destination as? UINavigationController,
                  let addItemListVC = navC.viewControllers.first as? AddItemTableViewController
            else{
                return
            }
            
            addItemListVC.delegate = self
        }
    }
    
    private func setupUI(){
        self.title = self.itemListTableVMV.inventoryName
        self.navigationItem.largeTitleDisplayMode = .never
    }
}

extension ItemListTableViewController: AddItemTableViewControlDelegate{
    func addItemTableViewControllerDidCancel(controller: UIViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func addItemTableViewControllerDidSave(controller: UIViewController, item: Item) {
        
        self.itemListTableVMV.addItem(item: item)
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
            controller.dismiss(animated: true, completion: nil)
        }
    }
}

extension ItemListTableViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.itemListTableVMV.numberOfSection
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemListTableVMV.numberOfRowsInSection(section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemListTableViewCell", for: indexPath)
        cell.textLabel?.text = self.itemListTableVMV.itemNameAtIndex(at: indexPath.row)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            self.itemListTableVMV.deleteItemAtIndex(index: indexPath.row)
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
}
