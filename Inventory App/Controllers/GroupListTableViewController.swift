//
//  GroupListTableViewController.swift
//  Inventory App
//
//  Created by Jansen Ducusin on 2/26/21.
//

import Foundation
import UIKit

class GroupListTableViewController: UITableViewController{
    
    private var groupListTableVM:GroupListTableViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.groupListTableVM = GroupListTableViewModel()
        self.setupUI()
        
        self.groupListTableVM.populateGroupList()
        self.groupListTableVM.delegate = self
    }
    
    @IBAction func logoutActionPressed(){
        self.groupListTableVM.signoutAccount()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddGroupTableViewController" {
            guard let navC = segue.destination as? UINavigationController,
                  let addGroupListTVC = navC.viewControllers.first as? AddGroupTableViewController else{
                fatalError("Segue error to AddGroupTableViewController")
            }
            
            addGroupListTVC.delegate = self
            
        }else if segue.identifier == "ItemListTableViewController"{
            guard let indexPath = self.tableView.indexPathForSelectedRow,
                  let tableVC = segue.destination as? UITableViewController,
                  let itemListTVC = tableVC as? ItemListTableViewController
            else {
                fatalError("Segue error to ItemListTableViewController")
            }
            
            itemListTVC.inventoryList = groupListTableVM.inventoryAtIndex(at: indexPath.row)
        }
    }
    
    private func setupUI(){
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
}

extension GroupListTableViewController: AddGroupTableViewControllerDelegate{
    func addGroupTableViewControllerDidSave(controller: UIViewController, groupName: String) {
        
        self.groupListTableVM.addNewGroup(withName:groupName)
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func addGroupTableViewControllerDidCancel(controller: UIViewController) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension GroupListTableViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.groupListTableVM.numberOfSection
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.groupListTableVM.numberOfRowsInSection(section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GroupListTableViewCell", for: indexPath)
        cell.textLabel?.text = self.groupListTableVM.inventoryAtIndex(at: indexPath.row).groupName
        
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            self.groupListTableVM.deleteGroupAtIndex(indexPath.row)
        }
    }
}

extension GroupListTableViewController: GroupListTableViewModelDelegate{
    func groupListTableViewModelReloadTable() {
        self.tableView.reloadData()
    }
    
    func groupListTableViewModelDismissView() {
        self.dismiss(animated: true, completion: nil)
    }
}
