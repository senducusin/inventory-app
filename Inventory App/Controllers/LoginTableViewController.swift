//
//  LoginTableViewController.swift
//  Inventory App
//
//  Created by Jansen Ducusin on 3/2/21.
//

import Foundation
import UIKit
import Firebase
import FirebaseAuth

class LoginTableViewController: UITableViewController{
    
    @IBOutlet weak var loginEmailTextField: UITextField!
    @IBOutlet weak var loginPasswordTextField: UITextField!
    
    @IBOutlet weak var registerEmailTextField: UITextField!
    @IBOutlet weak var registerPasswordTextField: UITextField!
    
    override func viewDidLoad() {
        self.setupPasswordFields()
        
        if let _ = Auth.auth().currentUser?.uid{
            self.performSegue(withIdentifier: "LoginSuccessSegue", sender: self)
        }
    }
    
    @IBAction func loginButtonPressed(){
        guard let email = self.loginEmailTextField.text,
              let password = self.loginPasswordTextField.text else {
            return
        }
        
        Auth.auth().signIn(withEmail: email, password: password){ user, error in
            if let error = error {
                self.showAlert(with: error.localizedDescription, alertType: .error)
                return
            }
            
            self.performSegue(withIdentifier: "LoginSuccessSegue", sender: self)
            self.clearLoginTextFields()
        }
        
    }
    
    @IBAction func registerButtonPressed(){
        guard let email = self.registerEmailTextField.text,
              let password = self.registerPasswordTextField.text else {
            return
        }
        
        Auth.auth().createUser(withEmail: email, password: password){ user, error  in
            if let error = error {
                self.showAlert(with: error.localizedDescription, alertType: .error)
            }else {
                self.showAlert(with: "Registration successful", alertType: .message)
                self.clearRegisterTextfields()
            }
        }
        
    }
    
    private func showAlert(with message: String, alertType: AlertType){
        let alertController = UIAlertController(title: alertType.rawValue, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func clearRegisterTextfields(){
        self.registerEmailTextField.text = ""
        self.registerPasswordTextField.text = ""
    }
    
    private func clearLoginTextFields(){
        self.loginEmailTextField.text = ""
        self.loginPasswordTextField.text = ""
    }
    
    private func setupPasswordFields(){
        self.loginPasswordTextField.isSecureTextEntry = true
        self.registerPasswordTextField.isSecureTextEntry = true
    }
    
}
