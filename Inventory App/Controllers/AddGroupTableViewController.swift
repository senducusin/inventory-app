//
//  AddGroupTableViewController.swift
//  Inventory App
//
//  Created by Jansen Ducusin on 2/26/21.
//

import Foundation
import UIKit

protocol  AddGroupTableViewControllerDelegate {
    func addGroupTableViewControllerDidSave(controller: UIViewController, groupName: String)
    func addGroupTableViewControllerDidCancel(controller: UIViewController)
}

class AddGroupTableViewController: UITableViewController{
    
    @IBOutlet weak var titleTextField: UITextField!
    
    var delegate: AddGroupTableViewControllerDelegate!
    
    @IBAction func save() {
        if let title = self.titleTextField.text {
            self.delegate.addGroupTableViewControllerDidSave(controller: self, groupName: title)
        }
    }
    
    @IBAction func cancel(){
        self.delegate.addGroupTableViewControllerDidCancel(controller: self)
    }
}
