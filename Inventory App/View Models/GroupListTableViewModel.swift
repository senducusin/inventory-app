//
//  GroupListTableViewModel.swift
//  Inventory App
//
//  Created by Jansen Ducusin on 2/26/21.
//

import Foundation
import Firebase
import FirebaseDatabase
import FirebaseAuth

protocol GroupListTableViewModelDelegate {
    func groupListTableViewModelReloadTable()
    func groupListTableViewModelDismissView()
}

class GroupListTableViewModel {
    var inventories: [Group]
    var delegate: GroupListTableViewModelDelegate?
    private var rootRef: DatabaseReference!
    private let user: User = Auth.auth().currentUser! // logically, at this point user is authenticated
    
    init(){
        self.inventories = [Group]()
        self.rootRef = Database.database().reference()
        
        Auth.auth().addStateDidChangeListener{ auth, user in
            if let _ = user{ }else {
                DispatchQueue.main.async {
                    self.delegate?.groupListTableViewModelDismissView()
                }
            }
        }
    }
    
    func signoutAccount(){
        try? Auth.auth().signOut()
    }
    
    func populateGroupList(){
        self.rootRef.child(self.user.emailWithoutSpecialCharacters).observe(.value){ snapshot in
            self.inventories.removeAll()
            let inventoriesDictionary = snapshot.value as? [String:Any] ?? [:]
            
            for (key, _) in inventoriesDictionary{
                if let inventoryDictionary = inventoriesDictionary[key] as? [String:Any]{
                    if let inventory = Group(inventoryDictionary){
                        self.inventories.append(inventory)
                    }
                }
            }
            
            DispatchQueue.main.async {
                self.delegate?.groupListTableViewModelReloadTable()
            }
        }
    }
    
    func addNewGroup(withName groupName:String){
        let inventory = Group(groupName: groupName)
        self.inventories.append(inventory)

        guard let email = self.user.email else {
            return
        }

        let userRef = self.rootRef.child(email.removeSpecialCharacters())

        let inventoriesRef = userRef.child(inventory.groupName)
        inventoriesRef.setValue(try! inventory.toDictionary())
    }
    
    func deleteGroupAtIndex(_ index:Int){
        let inventory = self.inventoryAtIndex(at: index)
        guard let email = self.user.email else {
            return
        }

        let userRef = self.rootRef.child(email.removeSpecialCharacters())
        let inventoryRef = userRef.child(inventory.groupName)
        inventoryRef.removeValue()
    }
}

extension GroupListTableViewModel{
    var numberOfSection: Int{
        return 1
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int{
        return self.inventories.count
    }
    
    func inventoryAtIndex(at index: Int) -> Group{
        let sortedInventories = self.sortedByGroupName()
        return sortedInventories[index]
    }
    
    private func sortedByGroupName() ->[Group]{
        return self.inventories.sorted(by: { $0.groupName.lowercased() < $1.groupName.lowercased()})
    }
}
