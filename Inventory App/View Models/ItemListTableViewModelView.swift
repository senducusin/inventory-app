//
//  ItemListTableViewModelView.swift
//  Inventory App
//
//  Created by Jansen Ducusin on 2/26/21.
//

import Foundation
import Firebase
import FirebaseDatabase
import FirebaseAuth

struct ItemListTableViewModelView{
    var inventory: Group
    private var rootRef: DatabaseReference!
    private let user: User = Auth.auth().currentUser!
    
    init(inventory:Group){
        self.inventory = inventory
        self.rootRef = Database.database().reference()
    }
    
    func addItem(item:Item){
        self.inventory.addItem(item)
        
        guard let email = self.user.email else {
            return
        }
        
        let userRef = self.rootRef.child(email.removeSpecialCharacters())
        let inventoriesRef = userRef.child(self.inventory.groupName)
        inventoriesRef.setValue(try! self.inventory.toDictionary())
    }
    
    func deleteItemAtIndex(index: Int){
        self.removeItemAt(index)
        
        guard let email = self.user.email else {
            return
        }
        
        let userRef = self.rootRef.child(email.removeSpecialCharacters())
        let inventoryRef = userRef.child(self.inventoryName)
        inventoryRef.setValue(try! self.inventory.toDictionary())
    }
}

extension ItemListTableViewModelView{
    var numberOfSection: Int{
        return 1
    }
    
    var inventoryName: String{
        return inventory.groupName
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int{
        return self.inventory.items.count
    }
    
    func itemNameAtIndex(at index: Int) -> String{
        let items = self.sortedByName()
        return items[index].name
    }
    
    private func sortedByName() -> [Item]{
        return self.inventory.items.sorted(by: {$0.name.lowercased() < $1.name.lowercased()})
    }
    
    func removeItemAt(_ index: Int){
        let items = self.sortedByName()
        let item = items[index]
        
        self.inventory.items = self.inventory.items.filter {$0.name != item.name}
    }
}
