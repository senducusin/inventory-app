//
//  Encodable+Extensions.swift
//  Inventory App
//
//  Created by Jansen Ducusin on 3/2/21.
//

import Foundation

extension Encodable{
    func toDictionary() throws -> [String:Any]{
        let data = try! JSONEncoder().encode(self)
        guard let dict = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any] else{
            throw NSError()
        }
        
        return dict
    }
}
