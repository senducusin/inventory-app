//
//  User+Extensions.swift
//  Inventory App
//
//  Created by Jansen Ducusin on 3/2/21.
//

import Foundation
import Firebase
import FirebaseAuth

extension User {
    var emailWithoutSpecialCharacters: String {
        guard let email = self.email else {
            fatalError("Unable to access the email of the user")
        }
        
        return email.removeSpecialCharacters()
    }
}
