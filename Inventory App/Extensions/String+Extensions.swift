//
//  String+Extensions.swift
//  Inventory App
//
//  Created by Jansen Ducusin on 3/2/21.
//

import Foundation

extension String {
    func removeSpecialCharacters() -> String {
        return self.components(separatedBy: CharacterSet.letters.inverted).joined()
    }
}
