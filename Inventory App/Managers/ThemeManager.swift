//
//  ThemeManager.swift
//  Inventory App
//
//  Created by Jansen Ducusin on 2/26/21.
//

import Foundation
import UIKit

class ThemeManager{
    static func setup(){
        let navigationBarAppearance = UINavigationBarAppearance()
        navigationBarAppearance.backgroundColor = UIColor(red: 0.64, green: 0.61, blue: 1.00, alpha: 1.00)
        
        navigationBarAppearance.titleTextAttributes = [.foregroundColor: UIColor(red: 0.96, green: 0.96, blue: 0.98, alpha: 1.00)]
        navigationBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor(red: 0.96, green: 0.96, blue: 0.98, alpha: 1.00)]
        
        UINavigationBar.appearance().tintColor = UIColor(red: 0.96, green: 0.96, blue: 0.98, alpha: 1.00)
        UINavigationBar.appearance().standardAppearance = navigationBarAppearance
        UINavigationBar.appearance().compactAppearance = navigationBarAppearance
        UINavigationBar.appearance().scrollEdgeAppearance = navigationBarAppearance
    }
}
