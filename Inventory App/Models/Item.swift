//
//  Item.swift
//  Inventory App
//
//  Created by Jansen Ducusin on 2/26/21.
//

import Foundation

class Item: Codable{
    var name: String
    
    init(name:String){
        self.name = name
    }
    
    init?(dictionary: JSONDictionary){
        guard let name = dictionary["name"] as? String else {
            return nil
        }
        self.name = name
    }
}

