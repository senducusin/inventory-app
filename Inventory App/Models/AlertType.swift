//
//  AlertType.swift
//  Inventory App
//
//  Created by Jansen Ducusin on 3/2/21.
//

import Foundation

enum AlertType: String{
    case error = "Error"
    case message = "Success"
}
