//
//  Group.swift
//  Inventory App
//
//  Created by Jansen Ducusin on 2/26/21.
//

import Foundation

typealias JSONDictionary = [String:Any]

class Group: Codable {
    
    var groupName: String!
    var items = [Item]()
    
    init(groupName:String){
        self.groupName = groupName
    }
    
    init?(_ dictionary: JSONDictionary){
        guard let groupName = dictionary["groupName"] as? String else {
            return nil
        }
        
        self.groupName = groupName
        
        let itemsDictionary = dictionary["items"] as? [JSONDictionary]
        
        if let dictionaries = itemsDictionary {
            self.items = dictionaries.compactMap(Item.init)
        }
    }
}
 
extension Group{
    func addItem(_ item:Item){
        items.append(item)
    }
    
    func removeItemAtIndex(_ index:Int){
        items.remove(at: index)
    }
}
