**Inventory App**

Organize your stuff by grouping them.

This is a sample app that utilizes Storyboards, UIKit, and Fireabase.

---

## Features

- Persistent logins
- Inventories are stored on individual accounts
- Account registration
- Account login/logout
- Data persistency using Firebase

---

## Demo

Data are stored into accounts, which prevents other users from accessing the data of another.

![](/Previews/login.png)

Items can be organized into groups, which can be created and will be arranged alphabetically in the group list view.

![](/Previews/add-group.gif)

![](/Previews/items.gif)

Items and groups can also be deleted by sliding the row to the left.

![](/Previews/delete.gif)
